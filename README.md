# Basic NodeJS API backend
API built for educational purpose.

# Requirements
 - NodeJS v10.x
 - MongoDB

## Installing node on Ubuntu based distro  
```
   curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -  
   sudo apt-get install -y nodejs
```
 
## Installing mongo from docker and configuring port redirect to host  
 ```
    docker pull mongo  
    docker run --name mongodb -p 27017:27017 -d mongo
 ```

 To run mongo after a machine restart  
 `docker start mongodb`  

 # Running project  
 First run `npm install` on node-api folder to install dependencies and then run `npm run dev`, this will bind the application to your 3001 port. You can use Postman for API manipulation.